import * as React from 'react';
import { LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import CountryList from './src/CountryList';
import CountryDetails from './src/CountryDetails';
import Map from './src/Map/Map';

const Stack = createNativeStackNavigator();

export default function App() {

  LogBox.ignoreAllLogs();

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="CountryList" component={CountryList} />
        <Stack.Screen name="CountryDetails" component={CountryDetails} />
        <Stack.Screen name="Map" component={Map} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}