import React from "react"
import { List } from 'react-content-loader/native'
import {
  View,
  FlatList,
  Dimensions,
  StyleSheet
} from 'react-native';
const { height, width } = Dimensions.get('screen');

const MyLoader = () => {

  return (
    <View >
      <FlatList data={[1, 1, 1, 1, 1, 1, 1]}
        renderItem={(item) => {
          return (
            <View style={styles.container1}>
              <List />
            </View>
          )
        }} />
    </View>
  )
}

const styles = StyleSheet.create({
  container1: {
    marginLeft: width * 0.05,
    marginTop: width * 0.05
  }
})

export default MyLoader