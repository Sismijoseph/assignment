import React, { useEffect, useState } from 'react';
import {
    Text,
    View,
    StyleSheet,
    FlatList,
    ActivityIndicator,
    TextInput,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import axios from 'axios';
import Icon from 'react-native-vector-icons/AntDesign';
import MyLoader from './Components/Loader';

const { height, width } = Dimensions.get('screen');
const baseURL = 'https://restcountries.com/v3.1/'

const CountryList = ({ navigation }) => {

    const [country, setCountry] = useState()
    const [Loading, setLoading] = useState(true)
    const [IsLoading, setIsLoading] = useState(false)
    const [searchValue, setSearchValue] = useState()

    useEffect(() => {
        getCountries()
    }, [])

    const getCountries = () => {
        axios.get(baseURL + 'all')
            .then(function (response) {
                setCountry(response?.data)
                setLoading(false)
            })
            .catch(function (error) {
                // handle error
                console.log(error);
                setLoading(false)
            })
    }

    const SearchCountries = (text) => {
        setIsLoading(true)
        axios.get(baseURL + `name/${text}`)
            .then(function (response) {
                if (response?.status == 200) {
                    setCountry(response?.data)
                    setIsLoading(false)
                }
            })
            .catch(function (error) {
                console.log(error);
                setIsLoading(false)
            })
    }


    return (
        <View style={styles.container1}>
            {Loading ?
                <View style={styles.LoaderView}>
                    <MyLoader />
                </View> :
                <View>
                    <Text style={styles.TextCountry}>Countries</Text>

                    <View style={styles.TextView}>
                        <TextInput
                            value={searchValue}
                            style={styles.textInput}
                            placeholder={'Region'}
                            placeholderTextColor={'#565657'}
                            onChangeText={(text) => SearchCountries(text)} />

                        <Icon
                            name={'down'}
                            size={12}
                            color={'#565657'}
                            style={{ marginTop: 15, marginRight: 10 }}
                        />
                    </View>


                    {IsLoading ?
                        <View style={{ marginTop: width * 0.05 }}>
                            <ActivityIndicator size={20} color={'#777877'} />
                        </View> :

                        <View style={{ marginBottom: width * 0.06 }}>
                            <FlatList
                                data={country}
                                renderItem={({ item, index }) => {
                                    return (
                                        <View style={{ alignItems: 'center' }} key={index}>
                                            <TouchableOpacity
                                                style={styles.touchBox}
                                                onPress={() => navigation.navigate('CountryDetails', { item: item })}>

                                                <Text style={styles.TextList}>{item?.name?.common}</Text>

                                                {item?.currencies && Object.keys(item?.currencies).map((key, index) => (
                                                    <View style={styles.ViewObj} key={index}>
                                                        <Text style={styles.TextList}>{item?.currencies[key]?.name}</Text>
                                                        <Text style={styles.TextList}>{item?.currencies[key]?.symbol}</Text>
                                                    </View>
                                                ))}

                                                <Text style={styles.TextList}>{item["capital"]?.[0]}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                }}
                            />
                        </View>}

                </View>}
        </View>
    )
};

const styles = StyleSheet.create({
    container1: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    LoaderView: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1
    },
    TextView: {
        borderWidth: 1,
        borderColor: '#565657',
        height: height * 0.05,
        flexDirection: 'row',
        marginLeft: width * 0.05,
        marginRight: width * 0.05,
        backgroundColor: '#fafaf7',
        justifyContent: 'space-between'
    },
    TextCountry: {
        fontSize: 18,
        color: '#000000',
        padding: width * 0.03,
        paddingTop: width * 0.08
    },
    TextList: {
        fontSize: 12,
        color: '#000000',
        paddingTop: 5,
        paddingLeft: width * 0.04,
        paddingBottom: 5
    },
    textInput: {
        fontSize: 12,
        color: '#000000',
        left: 5,
        flex: 1
    },
    touchBox: {
        width: width * 0.90,
        marginTop: width * 0.05,
        backgroundColor: '#fafaf7',
        justifyContent: 'center'
    },
    ViewObj: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginRight: width * 0.05
    }


})

export default CountryList;


