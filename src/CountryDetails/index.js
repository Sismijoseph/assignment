import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Dimensions
} from 'react-native';
const { height, width } = Dimensions.get('screen');

const CountryDetails = ({ route, navigation }) => {

    return (
        <View style={styles.container1}>
            <View style={styles.BoxView}>

                <Text style={styles.TextCountry1}>Country details</Text>

                <Text style={styles.TextList}>Country name : {route?.params?.item?.name?.common}</Text>
                <Text style={styles.TextList}>Capital : {route?.params?.item?.capital?.[0]}</Text>
                <Text style={styles.TextList}>Region : {route?.params?.item?.region}</Text>
                <Text style={styles.TextList}>Time zone : {route?.params?.item?.timezones?.[0]}</Text>
                <Text style={styles.TextList}>Population : {route?.params?.item?.population}</Text>

                {route?.params?.item?.name?.nativeName && Object.keys(route?.params?.item?.name?.nativeName).map((key, index) => (
                    <View key={index}>
                        <Text style={styles.TextList}>Native name : {route?.params?.item?.name?.nativeName?.[key]?.official}</Text>
                    </View>
                ))}
                <Text style={styles.TextList}>Calling code : {route?.params?.item?.idd?.root}</Text>

                <View style={{ alignItems: 'center', justifyContent: 'center' }}>

                    <TouchableOpacity onPress={() => navigation.navigate('Map',
                        {
                            lat: route?.params?.item?.latlng?.[0],
                            lng: route?.params?.item?.latlng?.[1]
                        })}
                        style={styles.TouchBtn}>
                        <Text style={styles.TextCountry}>View Map</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </View>
    )
};


const styles = StyleSheet.create({
    container1: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    TextCountry1: {
        fontSize: 18,
        color: '#000000',
        fontWeight: '700',
        paddingLeft: width * 0.05,
        paddingTop: width * 0.05
    },
    TextCountry: {
        fontSize: 18,
        color: '#000000',
        fontWeight: '700',
    },
    TextList: {
        fontSize: 12,
        color: '#000000',
        paddingTop: 10,
        paddingLeft: width * 0.05
    },
    BoxView: {
        elevation: 5,
        width: width * 0.9,
        backgroundColor: '#FFFFFF',
        margin: 5
    },
    TouchBtn: {
        backgroundColor: '#d1cfcf',
        width: width * 0.3,
        height: height * 0.05,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: width * 0.05,
        marginTop: width * 0.05
    }


})

export default CountryDetails;