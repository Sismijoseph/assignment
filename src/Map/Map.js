import React from 'react';
import { View, StyleSheet } from 'react-native';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';

const Map = ({ route }) => {

    return (
        <View style={styles.MainContainer}>
            <MapView
                style={styles.mapStyle}
                showsUserLocation={false}
                zoomEnabled={true}
                zoomControlEnabled={true}
                initialRegion={{
                    latitude: route?.params?.lat,
                    longitude: route?.params?.lng,
                    latitudeDelta: 1,
                    longitudeDelta: 1,
                }}>

                <Marker
                    coordinate={{
                        latitude: route?.params?.lat, longitude: route?.params?.lng
                    }}
                />
            </MapView>
        </View>
    )
};

const styles = StyleSheet.create({
    MainContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    mapStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
});

export default Map;